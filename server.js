/* global Promise */
const express = require('express');
const expressHandlebars = require('express-handlebars');
const axios = require('axios');
const { format } = require('date-fns');

const PORT = 8080;

const server = express();

server.engine('handlebars', expressHandlebars({defaultLayout: 'main'}));
server.use('/assets', express.static('assets'));
server.set('view engine', 'handlebars');

const getResults = query =>
  axios(`http://www.reddit.com/u/${query}/comments.json`)
    .then(({ data: { data: { children } } }) =>
      children.map(({ data }) =>
        ({ ...data, date: format(data.created * 1000, 'YYYY-MM-DD H:mm:ss') })
      )
    )
    .catch((e) => Promise.resolve(e));

server.get('/', (req, res) => {
  console.log(`visitor at ${JSON.stringify(req.path)}`);

  const query = req.query.q;

  if (query) {
    getResults(query)
      .then(results =>
        res.render('home', { query, results })
      );
  } else {
    res.render('home');
  }
});


server.listen(PORT, () => {
  console.log(`Running on http://localhost:${PORT}`);
});
